
import React, { Component } from 'react';
import './App.css';
import quizData from "./quizData";


class App extends Component {
  state = {
    currentQuestion: 0,
    myAnswer: null,
    options: [],
    score: 0,
    disabled: true,
    isEnd: false
  };

  loadQuizData = () => {
     this.setState(() => {
      return {
        questions: quizData[this.state.currentQuestion].question,
        answer: quizData[this.state.currentQuestion].answer,
        options: quizData[this.state.currentQuestion].options
      };
    }); 
  };

  componentDidMount() {
    this.loadQuizData();
  }

  
  render(){
   
    return(
      <div className="App container">
      <div className="row">
        <div className="col-12 mx-auto text-center">
        <h1>Quiz app</h1>
        </div>
      </div>
      

      <div className="row">
        <div className="col-12 text-center">
          <h4>Question</h4>
        </div>
      </div>

      <div className="row text-center">
        <div className="col-6 p-3">
          <button>Question 1</button>
        </div>
        <div className="col-6 p-3">
          <button>Question 2</button>
        </div>
        <div className="col-6 p-3">
          <button>Question 3</button>
        </div>
        <div className="col-6 p-3">
          <button>Question 4</button>
        </div>
      </div>

      <div className="row text-center">
        <div className="col-4 mx-auto">
          <button>Suivant</button>
        </div>
      </div>

      </div>
    )
  };
 
};

export default App;
